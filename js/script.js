Vue.use(VueResource);
var app = new Vue({
    el: '#app',
    data: {
        item: {
            name: '',
            adress: ''
        },
        list: [{
            name: '',
            address: ''
        }],
        pos: []
    },
    methods: {
        addMark: function(item, event) {
            event.preventDefault();
            if (item.name != "" && item.address != "") {
                this.list.push({
                    name: item.name,
                    address: item.address
                });

                var listPlaces = localStorage.getItem('listPlaces');

                this.getGeopositionByAddress(item.address, function(data) {

                    if (listPlaces == null) {
                        listPlaces = [];
                        updateLocalStorage(listPlaces, 'listPlaces', {
                            name: item.name,
                            address: item.address,
                            pos: data
                        });
                    } else {
                        listPlaces = JSON.parse(listPlaces);
                        updateLocalStorage(listPlaces, 'listPlaces', {
                            name: item.name,
                            address: item.address,
                            pos: data
                        });
                    }
                });
            }

            function updateLocalStorage(array, key, object) {
                array.push(object);
                array = JSON.stringify(array);
                localStorage.setItem(key, array);
            }
        },
        mapInit: function(listPlaces) {
            var myMap,
                placemark,
                myPlacemark,
                myGeoObjects;

            ymaps.ready(init);

            function init() {
                myMap = new ymaps.Map("map", {
                    center: [59.939095, 30.315868],
                    zoom: 10
                });

                if (listPlaces != null && listPlaces.length != 0) {
                    var myGeoObjects = new ymaps.GeoObjectCollection({}, {
                        geodesic: true
                    });

                    for (var i = 0; i < listPlaces.length; i++) {
                        placemark = new ymaps.Placemark(listPlaces[i].pos.reverse(), {
                            hintContent: listPlaces[i].name,
                            balloonContent: listPlaces[i].address
                        });

                        myGeoObjects.add(placemark);
                    }
                    myMap.geoObjects.add(myGeoObjects);
                    if (listPlaces.length != 1) {
                        myMap.setBounds(myGeoObjects.getBounds());
                    }
                }

                $('#addMark').on('click', function() {
                    var name = $('#item-name').val();
                    var address = $('#item-address').val();

                    Vue.http.get('https://geocode-maps.yandex.ru/1.x/?format=json&geocode=' + address)
                        .then(function(data) {
                            if (data.body.response.GeoObjectCollection.featureMember.length == 0) {
                                // alert('Нет такого города :)');
                            } else {
                                var markerPos = data.data.response.GeoObjectCollection.featureMember["0"].GeoObject.Point.pos;
                                markerPos = markerPos.split(' ');

                                myPlacemark = new ymaps.Placemark(markerPos.reverse(), {
                                    hintContent: name,
                                    balloonContent: address
                                });
                                myMap.geoObjects.add(myPlacemark);
                                myMap.setCenter(markerPos, 13);
                            }
                        });
                });

                $('input[type=checkbox]').on('change', function() {
                    var tr = $(this).parent().parent();
                    var i = $(this).val();
                    if ($(this).prop('checked')) {
                        tr.addClass('bg-success');

                        myPlacemark = new ymaps.Placemark(listPlaces[i].pos, {
                            hintContent: 'name',
                            balloonContent: 'address'

                        }, {
                            preset: "islands#dotCircleIcon",
                            iconColor: 'green'
                        });
                        myGeoObjects.remove(i);
                        myGeoObjects.add(myPlacemark);
                        myMap.geoObjects.add(myGeoObjects);
                    } else {
                        tr.removeClass('bg-success');
                        myPlacemark = new ymaps.Placemark(listPlaces[i].pos, {
                            hintContent: 'name',
                            balloonContent: 'address'

                        });
                        myGeoObjects.remove(i);
                        myGeoObjects.add(myPlacemark);
                        myMap.geoObjects.add(myGeoObjects);
                    }
                });

                $('.show-place').on('click', function() {
                    var tr = $(this).parent().parent();
                    var index = $(tr).attr('index');

                    myMap.setCenter(listPlaces[index].pos, 13);
                });

                $('.remove').on('click', function() {
                    var tr = $(this).parent().parent();
                    var i = $(tr).attr('index');

                    $(tr).hide('slow');
                    console.log(listPlaces);
                    listPlaces.splice(i, 1);
                    listPlaces = JSON.stringify(listPlaces);
                    localStorage.setItem('listPlaces', listPlaces);
                    console.log(i);
                    console.log(myGeoObjects.toArray(i));
                    myGeoObjects.remove(i);
                });
            }
        },
        getGeopositionByAddress: function(address, cb) {
            this.$http.get('https://geocode-maps.yandex.ru/1.x/?format=json&geocode=' + address)
                .then(response => {
                    if (response.body.response.GeoObjectCollection.featureMember.length == 0) {
                        alert('Нет такого адреса :)');
                    } else {
                        var markerPos = response.data.response.GeoObjectCollection.featureMember["0"].GeoObject.Point.pos;
                        markerPos = markerPos.split(' ');
                        cb(markerPos);
                    }
                }, response => {
                    console.log(response);
                });
        }
    },
    mounted() {
        var listPlaces = JSON.parse(localStorage.getItem('listPlaces'));
        if (listPlaces != null) {
            this.list = listPlaces;
        }

        this.mapInit(listPlaces);
    },
    created() {

    }
})